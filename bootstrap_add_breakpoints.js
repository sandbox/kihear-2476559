(function ($) {

Drupal.behaviors.bootstrap_add_breakpoints = {

  attach: function (context, settings) {
  var bs_add_breakpoints_breakpoint;
  bs_add_breakpoints_addCurrentBreakpoint();
  
// Remove added viewport classes and add current class to body
  function bs_add_breakpoints_addCurrentBreakpoint() {
      if( bs_add_breakpoints_isBreakpoint('xs')) { bs_add_breakpoints_breakpoint = 'xs'; }
      if( bs_add_breakpoints_isBreakpoint('sm')) { bs_add_breakpoints_breakpoint = 'sm'; }
      if( bs_add_breakpoints_isBreakpoint('md')) { bs_add_breakpoints_breakpoint = 'md'; }
      if( bs_add_breakpoints_isBreakpoint('lg')) { bs_add_breakpoints_breakpoint = 'lg'; }
      $('body').removeClass('xs');
      $('body').removeClass('sm');
      $('body').removeClass('md');
      $('body').removeClass('lg');
      $('body').addClass(bs_add_breakpoints_breakpoint);	  
  }

// Determine which bootstrap helper div class is active
  function bs_add_breakpoints_isBreakpoint(bs_add_breakpoints_alias) {
    return $('.device-' + bs_add_breakpoints_alias).is(':visible');
  }

// Call window resize function only once resizing has finished
var bs_add_breakpoints_waitForFinalEvent = (function () {
	var bs_add_breakpoints_timers = {};
	return function (bs_add_breakpoints_callback, bs_add_breakpoints_ms, bs_add_breakpoints_uniqueId) {
		if (!bs_add_breakpoints_uniqueId) {
			bs_add_breakpoints_uniqueId = "Determining correct breakpoint...";
		}
		if (bs_add_breakpoints_timers[bs_add_breakpoints_uniqueId]) {
			clearTimeout (bs_add_breakpoints_timers[bs_add_breakpoints_uniqueId]);
		}
		bs_add_breakpoints_timers[bs_add_breakpoints_uniqueId] = setTimeout(bs_add_breakpoints_callback, bs_add_breakpoints_ms);
	};
})();

  var bs_add_breakpoints_fullDateString = new Date();

  $(window).resize(function () {
    bs_add_breakpoints_waitForFinalEvent( function() {
    bs_add_breakpoints_addCurrentBreakpoint();
    }, 100, bs_add_breakpoints_fullDateString.getTime());
  });  // resize
  } // attach
}; // behavior.setbreakpoint
}(jQuery));